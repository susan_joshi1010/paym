import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Payment } from '../models/payment.model';

@Injectable()
export class PaymentDetailService {
  apiUrl = 'http://localhost:8080/payments-system';

  constructor(private http: HttpClient) {}

  getPaymentDetail(id): Observable<Payment[]>{
    return this.http.get<Payment[]>(`${this.apiUrl}/payment/${id}`);
  }

}
