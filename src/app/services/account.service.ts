import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Account } from '../models/account.model';

@Injectable()
export class AccountService {
  apiUrl = 'http://localhost:8080/payments-system';

  constructor(private http: HttpClient) {}

  getAccounts(): Observable<Account[]>{
    return this.http.get<Account[]>(`${this.apiUrl}/account`);
  }
}
