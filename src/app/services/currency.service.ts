import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Currency } from '../models/currency.model';

@Injectable()
export class CurrencyService {

  apiUrl = 'http://localhost:8080/payments-system';

  constructor(private http: HttpClient) {}

  getCurrencies(): Observable<Currency[]>{
    return this.http.get<Currency[]>(`${this.apiUrl}/currency`);
  }

}
