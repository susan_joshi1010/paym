import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Account } from '../models/account.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AddAccountService {

  apiUrl = 'http://localhost:8080/payments-system';

  constructor(private http: HttpClient) {}

  storeAccount(account): Observable<Account[]>{
    return this.http.post<Account[]>(`${this.apiUrl}/account/add`, account)
                                    .catch((error) => {
                                      return Observable.throw(error || "Server Error")
                                    });
  }

}
