import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Payment } from '../models/payment.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class MakePaymentService {

  apiUrl = 'http://localhost:8080/payments-system';

  constructor(private http: HttpClient) {}

  makePayment(payment): Observable<Payment[]>{
    return this.http.post<Payment[]>(`${this.apiUrl}/payment/add`, payment)
                                    .catch((error) => {
                                      return Observable.throw(error || "Server Error")
                                    });
  }

}
