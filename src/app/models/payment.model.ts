export interface Payment{
  id: string,
  sourceAccountNumber: number,
  destinationAccountNumber: number,
  amount: number,
  currencyCode: string,
  paymentDescription: string
}
