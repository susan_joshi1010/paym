export interface Account{
  id: string,
  accountNumber: number,
  accountHolderName: string,
  accountHolderPhoneNumber: string,
  accountDescription: string
}
