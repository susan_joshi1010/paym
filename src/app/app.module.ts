import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AccountService } from './services/account.service';
import { CurrencyService } from './services/currency.service';
import { AccountDetailService } from './services/account-detail.service';
import { AddAccountService } from './services/add-account.service';
import { PaymentService } from './services/payment.service';
import { PaymentDetailService } from './services/payment-detail.service';
import { MakePaymentService } from './services/make-payment.service';

import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {  MatTableModule,
          MatButtonModule,
          MatFormFieldModule,
          MatInputModule,
          MatSelectModule
        } from '@angular/material';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './home/home.component';
import { AccountComponent } from './components/account/account.component';
import { AccountDetailComponent } from './components/account-detail/account-detail.component';
import { CurrencyComponent } from './components/currency/currency.component';
import { PaymentComponent } from './components/payment/payment.component';
import { PaymentDetailComponent } from './components/payment-detail/payment-detail.component';
import { LoginComponent } from './components/auth/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AccountComponent,
    AccountDetailComponent,
    CurrencyComponent,
    PaymentComponent,
    PaymentDetailComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    MatTableModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    BrowserAnimationsModule
  ],
  providers: [
    AccountService,
    AccountDetailService,
    AddAccountService,
    CurrencyService,
    PaymentService,
    PaymentDetailService,
    MakePaymentService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
