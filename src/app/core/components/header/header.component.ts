import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <nav class="navbar is-dark">

      <!-- logo -->
      <div class="navbar-brand">
        <a class="navbar-item">
          <img src="assets/img/logo.jpg">
        </a>
      </div>

      <!-- menu -->
      <div class="navbar-menu">
        <div class="navbar-start">
          <a class="navbar-item" routerLink="home">Home</a>
          <a class="navbar-item" routerLink="account">Account</a>
          <a class="navbar-item" routerLink="payment">Payment</a>
          <a class="navbar-item" routerLink="currency">Currency</a>
        </div>
      </div>

      <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Welcome <i>John Doe</i>
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item">Profile</a>
          <a class="dropdown-item">Settings</a>
          <div class="dropdown-divider"></div>
            <a routerLink="/" class="dropdown-item">Logout</a>
        </div>
      </div>
    </nav>
  `,
  styles: []
})
export class HeaderComponent implements OnInit {
  constructor() {}
  ngOnInit() {}
}
