import { Component } from '@angular/core';
import { Router }  from "@angular/router";

@Component({
  selector: 'app-root',
  template: `
    <!-- header -->
    <app-header *ngIf="router.url !== '/'"></app-header>

    <!-- routes will be rendered here -->
    <router-outlet></router-outlet>

    <!-- footer -->
    <app-footer *ngIf="router.url !== '/'"></app-footer>
  `,
  styles: []
})
export class AppComponent {

  constructor(public router: Router){}

}
