import { Component, OnInit } from '@angular/core';
import { PaymentDetailService } from '../../services/payment-detail.service';
import { MakePaymentService } from '../../services/make-payment.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Payment } from '../../models/payment.model';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyService } from '../../services/currency.service';

@Component({
  selector: 'app-payment-detail',
  template: `
    <section class="hero is-primary is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">
              {{ make_payment? "Make Payment" : "Payment Details"}}
            <a routerLink="/payment" class="pull-right"><button mat-raised-button class="btn btn-success" > Payment</button></a>
          </h1>
        </div>
      </div>
      <div class="example-button-row">
      </div>
    </section>
    <div class="container form">
      <div *ngIf="errorMsg!=''" class="alert alert-danger" role="alert">
        {{errorMsg}}
      </div>
      <div *ngIf="paymentStatus" class="alert alert-success" role="alert">
        Payment Made Sucessfully.
      </div>
      <div class="form-group">
      <label for="source_account_number">Source Account No.</label>
      <input type="number" class="form-control" id="source_account_number" disabled={{!make_payment}} [(ngModel)]="payment_details.sourceAccountNumber" placeholder="Enter Source Account No.">
      </div>
      <div class="form-group">
      <label for="destination_account_number">Destination Account No.</label>
      <input type="number" class="form-control" id="destination_account_number" disabled={{!make_payment}} [(ngModel)]="payment_details.destinationAccountNumber" placeholder="Enter Destination Account No.">
      </div>
      <div class="form-group">
        <label for="currency">Currency</label>
        <select class="form-control" id="currency" disabled={{!make_payment}} [(ngModel)]="payment_details.currencyCode">
          <option value="">Select a currency</option>
          <option *ngFor="let currency of currencies" [value]="currency">{{currency}}</option>
        </select>
      </div>
      <div class="form-group">
        <label for="amount">Amount</label>
        <input type="nunmber" class="form-control" id="amount" disabled={{!make_payment}} [(ngModel)]="payment_details.amount" placeholder="Amount">
      </div>
      <div class="form-group">
        <label for="description">Description</label>
         <textarea class="form-control" rows="3" id="description" disabled={{!make_payment}} [(ngModel)]="payment_details.paymentDescription" placeholder="Enter Description"></textarea>
      </div>
      <button type="submit" class="btn btn-primary" *ngIf="make_payment" (click)="makePayment()">Make Payment</button>
    </div>
  `,
  styles: [`
    .form{
      margin-top: 30px;
      margin-bottom: 30px;
    }
  `]
})
export class PaymentDetailComponent implements OnInit {

  constructor(private currencyService: CurrencyService, private paymentDetailService: PaymentDetailService, private route: ActivatedRoute, private makePaymentService: MakePaymentService) { }

  payment_details:object = {
    currencyCode : ""
  };
  id:string;
  errorMsg:string="";
  make_payment:boolean=false;
  paymentStatus:boolean=false;
  currencies:object = {};

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
    })

    this.fetchCurrencies();

    if(this.id!='add'){
      this.fetchPaymentDetails();
    }else{
      this.make_payment=true;
    }
  }

  fetchCurrencies() {
    this.currencyService.getCurrencies().subscribe(data => {
      this.currencies = data;
    });
  }

  fetchPaymentDetails() {
    this.paymentDetailService.getPaymentDetail(this.id).subscribe(data => {
      this.payment_details = data;
    });
  }

  makePayment() {
    this.makePaymentService.makePayment(this.payment_details).subscribe(data => {
      this.errorMsg = "";
      this.payment_details={
        currencyCode : ""
      };
      this.paymentStatus=true;
    }, (err)=>{
      this.paymentStatus=false;
      this.errorMsg = err.error;
    });
  }

}
