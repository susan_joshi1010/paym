import { Component, OnInit } from '@angular/core';
import { AccountDetailService } from '../../services/account-detail.service';
import { AddAccountService } from '../../services/add-account.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Account } from '../../models/account.model';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-account-detail',
  template: `
  <section class="hero is-primary is-bold">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
            {{ create_account? "Create Account" : "Account Details"}}
          <a routerLink="/account" class="pull-right"><button mat-raised-button class="btn btn-success" > Account</button></a>
        </h1>
      </div>
    </div>
    <div class="example-button-row">
    </div>
  </section>
  <div class="container form">
    <div *ngIf="errorMsg!=''" class="alert alert-danger" role="alert">
      {{errorMsg}}
    </div>
    <div *ngIf="saveSuccess" class="alert alert-success" role="alert">
      Account Sucessfully Created.
    </div>
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" disabled={{!create_account}} [(ngModel)]="account_details.accountHolderName" placeholder="Enter Name">
    </div>
    <div class="form-group">
    <label for="account_number">Account No.</label>
    <input type="text" class="form-control" id="account_number" disabled={{!create_account}} [(ngModel)]="account_details.accountNumber" placeholder="Enter Account No.">
    </div>
    <div class="form-group">
      <label for="contact">Contact</label>
      <input type="text" class="form-control" id="contact" disabled={{!create_account}} [(ngModel)]="account_details.accountHolderPhoneNumber" placeholder="Enter Contact No.">
    </div>
    <div class="form-group">
      <label for="description">Description</label>
       <textarea class="form-control" rows="3" id="description" disabled={{!create_account}} [(ngModel)]="account_details.accountDescription" placeholder="Enter Description"></textarea>
    </div>
    <button type="submit" class="btn btn-primary" *ngIf="create_account" (click)="createAccount()">Submit</button>
  </div>
  `,
  styles: [`
    .form{
      margin-top: 30px;
      margin-bottom: 30px;
    }
    `]
})
export class AccountDetailComponent implements OnInit {

  constructor(private accountDetailService: AccountDetailService, private route: ActivatedRoute, private addAccountService: AddAccountService) { }

  account_details:object = {};
  id:string;
  errorMsg:string="";
  create_account:boolean=false;
  saveSuccess:boolean=false;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
    })
    if(this.id!='add'){
      this.fetchAccountDetails();
    }else{
      this.create_account=true;
    }
  }

  fetchAccountDetails() {
    this.accountDetailService.getAccountsDetail(this.id).subscribe(data => {
      this.account_details = data;
    });
  }

  createAccount() {
    this.addAccountService.storeAccount(this.account_details).subscribe(data => {
      this.errorMsg = "";
      this.account_details={};
      this.saveSuccess=true;
      // this.toastr.success('Account Created!', 'Success!');
    }, (err)=>{
      this.errorMsg = err.error;
      this.saveSuccess=false;
    });
  }
}
