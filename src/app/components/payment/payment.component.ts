import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../../services/payment.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Payment } from '../../models/payment.model';
import { Router } from '@angular/router'

@Component({
  selector: 'app-payment',
  template: `
    <section class="hero is-primary is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">
            Payment
            <a routerLink="/payment/add" class="pull-right"><button mat-raised-button color="primary" class="btn btn-success" > Make Payment</button></a>
          </h1>
        </div>
      </div>
      <div class="example-button-row">
      </div>
    </section>
    <mat-table [dataSource]="dataSource" class="mat-elevation-z8">
    <!-- Source Account Column -->
    <ng-container matColumnDef="sourceAccountNumber">
      <mat-header-cell *matHeaderCellDef> Source Account No. </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.sourceAccountNumber}} </mat-cell>
    </ng-container>

    <!-- Destination Account no Column -->
    <ng-container matColumnDef="destinationAccountNumber">
      <mat-header-cell *matHeaderCellDef> Destination Account No. </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.destinationAccountNumber}} </mat-cell>
    </ng-container>

    <!-- Amount Column -->
    <ng-container matColumnDef="amount">
      <mat-header-cell *matHeaderCellDef> Amount </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.amount}} </mat-cell>
    </ng-container>

    <!-- Currency Code Column -->
    <ng-container matColumnDef="currencyCode">
      <mat-header-cell *matHeaderCellDef> Currency </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.currencyCode}} </mat-cell>
    </ng-container>

    <!-- Payment Description Column -->
    <ng-container matColumnDef="paymentDescription">
      <mat-header-cell *matHeaderCellDef> Description </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.paymentDescription}} </mat-cell>
    </ng-container>

    <!-- Action Column -->
    <ng-container matColumnDef="actions">
      <th mat-header-cell *matHeaderCellDef> Action </th>
      <td mat-cell *matCellDef="let element">
        <button mat-raised-button (click)="paymentDetails($event, element.id)">View</button>
      </td>
    </ng-container>

    <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
    <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>
    </mat-table>
  `,
  styles: [`
    table {
      width: 100%;
    }
    .example-button-row button,
    .example-button-row a {
      margin-right: 8px;
    }
  `]
})
export class PaymentComponent implements OnInit {

  dataSource =  new PaymentDataSource(this.paymentService);
  displayedColumns = ["sourceAccountNumber", "destinationAccountNumber", "amount", "currencyCode", "paymentDescription", "actions"];

  constructor(private paymentService: PaymentService, private router: Router) { }

  ngOnInit() {
  }

  public paymentDetails(event, id) {
    this.router.navigate(['payment',id]);
  }

}

export class PaymentDataSource extends DataSource<any> {
  constructor(private paymentService: PaymentService) {
    super();
  }

  connect(): Observable<Payment[]> {
    return this.paymentService.getPayments();
  }

  disconnect() {}
}
