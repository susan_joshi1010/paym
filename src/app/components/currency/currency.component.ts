import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../../services/currency.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Currency } from '../../models/currency.model';
import { Router } from '@angular/router'

@Component({
  selector: 'app-currency',
  template: `
    <section class="hero is-primary is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">Currency (<i class="fa fa-dollar" aria-hidden="true"></i>)</h1>
        </div>
      </div>
      <div class="example-button-row">
      </div>
    </section>
    <div class="container">
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Currency</th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="let currency of currencies; let i = index">
            <th scope="row">{{i + 1}}</th>
            <td>{{currency}}</td>
          </tr>
        </tbody>
      </table>
    </div>
  `,
  styles: []
})
export class CurrencyComponent implements OnInit {

  currencies = {};

  constructor(private currencyService: CurrencyService) { }

  ngOnInit() {
    this.fetchCurrency();
  }

  fetchCurrency() {
    this.currencyService.getCurrencies().subscribe(data => {
      this.currencies = data;
    });
  }

}
