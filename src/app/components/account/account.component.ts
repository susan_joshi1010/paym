import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../services/account.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Account } from '../../models/account.model';
import { Router } from '@angular/router'

@Component({
  selector: 'app-account',
  template: `
    <section class="hero is-primary is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">
              Account
            <a routerLink="/account/add" class="pull-right"><button mat-raised-button color="primary" class="btn btn-success" > Add Account</button></a>
          </h1>
        </div>
      </div>
      <div class="example-button-row">
      </div>
    </section>
    <mat-table [dataSource]="dataSource" class="mat-elevation-z8">
    <!-- Name Column -->
    <ng-container matColumnDef="accountHolderName">
      <mat-header-cell *matHeaderCellDef> Name </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.accountHolderName}} </mat-cell>
    </ng-container>

    <!-- Account no Column -->
    <ng-container matColumnDef="accountNumber">
      <mat-header-cell *matHeaderCellDef> Account No </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.accountNumber}} </mat-cell>
    </ng-container>

    <!-- Contact Column -->
    <ng-container matColumnDef="accountHolderPhoneNumber">
      <mat-header-cell *matHeaderCellDef> Contact </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.accountHolderPhoneNumber}} </mat-cell>
    </ng-container>

    <!-- Description Column -->
    <ng-container matColumnDef="accountDescription">
      <mat-header-cell *matHeaderCellDef> Description </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{element.accountDescription}} </mat-cell>
    </ng-container>

    <!-- Action Column -->
    <ng-container matColumnDef="actions">
      <th mat-header-cell *matHeaderCellDef> Action </th>
      <td mat-cell *matCellDef="let element">
        <button mat-raised-button (click)="accountDetails($event, element.id)">View</button>
      </td>
    </ng-container>

    <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
    <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>
    </mat-table>
  `,
  styles: [`
    table {
      width: 100%;
    }
    .example-button-row button,
    .example-button-row a {
      margin-right: 8px;
    }
  `]
})


export class AccountComponent implements OnInit {

  dataSource =  new AccountDataSource(this.accountService);
  displayedColumns = ['accountHolderName', 'accountNumber', 'accountHolderPhoneNumber', 'accountDescription', 'actions'];

  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit() {
  }

  public accountDetails(event, id) {
    this.router.navigate(['account',id]);
  }
}

export class AccountDataSource extends DataSource<any> {
  constructor(private accountService: AccountService) {
    super();
  }

  connect(): Observable<Account[]> {
    return this.accountService.getAccounts();
  }

  disconnect() {}
}
